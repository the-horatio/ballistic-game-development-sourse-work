#include "Stone.h"
#include <math.h>
#define PI 3.14159265
#define DEGtoRAD PI/180.0
#define G 9.81 

Stone::Stone()
{
	m_Position.x = 70;		//  ���������
	m_Position.y = 850;		//  ���������� ���������
	
	//////////////////////////////////////////////////
	m_Texture.loadFromFile("assets/img/Stone.png");	//
	m_Sprite.setTexture(m_Texture);					//
	m_Sprite.setTextureRect(IntRect(0, 0, 22, 22));	//  ��������� ���������� �����(�����������, ����������)
	m_Sprite.setScale(1, 1);						//
	m_Sprite.setPosition(m_Position);				//
	//////////////////////////////////////////////////
	
	//////////////////////////
	m_Angle = 0.0;			//
	m_Velocity = 0.0;		//
	m_Time = 0.f;			//  �������� ��� ����
	m_currentFrame = 0.f;	//
	//////////////////////////

	m_Bounds = m_Sprite.getGlobalBounds(); // ������������� ������� �������

	m_currentDirection = Player::FIRST;	// ������������� ��������� ����������� ������ (��� ������� ������)
	isFlying = false;					// ���� ����������� ���������� �� �������
}

Sprite Stone::getButtonText() const
{
	return m_Sprite;	// �������� ������ �����
}

FloatRect Stone::getBounds() const
{
	return m_Bounds;	// �������� ������� �����
}

int Stone::getAngle() const
{
	return (int)m_Angle;	// �������� ����������� ���� �����
}

int Stone::getVelocity() const
{
	return (int)m_Velocity;	// �������� ����������� �������� �����
}

void Stone::addAngle()
{
	if (m_Angle < 90)		// ���� � ��������, ������� ��������� �������. 
		m_Angle += 0.15;	// ���������� �� �������� �� ����
	return;
}

void Stone::subAngle()
{
	if (m_Angle > 0.15)		// ���� � ��������, ������� ��������� �������.
		m_Angle -= 0.15;	// ���������� �� �������� ��� ����
	return;
}

void Stone::addVelocity()
{
	if (m_Velocity < 20)	// �������� � �������� ��������
		m_Velocity += 0.1;	//
	return;
}

void Stone::subVelocity()
{
	if (m_Velocity > 0.055)	// �������� � �������� ��������
		m_Velocity -= 0.1;	//
	return;
}

void Stone::setDirection(Player direction)
{
	m_currentDirection = direction;	// ������������� ����������� �������
	return;
}

void Stone::setPosition(Player player)
{
	if (player == Player::FIRST)	// �������� ��������� �������
	{
		m_Position.x = 70;			// ��������� ������� �����
		m_Position.y = 850;			// ��� ������ ����������
	}
	else
	{
		m_Position.x = 1800;		// ��������� ������� �����
		m_Position.y = 850;			// ��� ������ ����������
	}
	return;
}

void Stone::flight(float dtAsSeconds)	// �������� ���������� ����� �� �������� ����
{	
	m_Time += dtAsSeconds;				// ������� ����� ����� ������
	m_currentFrame += 5*dtAsSeconds;	// ���������� ����� ����
	
	if (m_currentFrame < 8)				// ���� �� ��� ����� ����������
	{
		m_Sprite.setTextureRect(IntRect(int(m_currentFrame) * 22, 0, 22, 22));	// ��������� ����
	}
	else
	{
		m_currentFrame = 0;	// ������������ � ���������� �����
		m_Sprite.setTextureRect(IntRect(m_currentFrame, 0, 22, 22));
	}

	int direction = (m_currentDirection == Player::FIRST) ? 1 : -1; // ����������� ����������� � ���������
																	// � ����������� �� �����������

	if (m_Position.x < VideoMode::getDesktopMode().width && m_Position.y < 925)		// �������� �� ����� �� �������
	{																				// ��� ������������ � ������
		m_Position.x += direction * (int)m_Velocity * cos((int)m_Angle * DEGtoRAD) * m_Time;	// ������� ����� ������� �����
		m_Position.y -= (int)m_Velocity * sin((int)m_Angle * DEGtoRAD) * m_Time - G * m_Time * m_Time / 2.0;
		m_Sprite.setPosition(m_Position);
		m_Bounds = m_Sprite.getGlobalBounds();
	}
	else
		isFlying = false; // ���� ������ �������� ��� �� ��������� ������, �� ����� �� �� �����

	return;
}
