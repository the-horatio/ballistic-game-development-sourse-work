#include "engine.h"
#include "Menu.h"

Menu::Menu()
{
	m_backgroundTex.loadFromFile("assets/img/menu_background.jpg");	// ��������� ������� �������� 
	m_backgroundSpr.setTexture(m_backgroundTex);					// 
	
	m_logoTex.loadFromFile("assets/img/menu_logo_2.png");	//
	m_logoSpr.setTexture(m_logoTex);						// ��������� ����
	m_logoSpr.setPosition(Vector2f(140, 110));				//

	SELECTED_BUTTON = (int)buttons::NEWGAME;	// ������������� ������� ������ ����� ����

	m_menuMusic.openFromFile("assets/music/menu_music.ogg");	//
	m_menuMusic.setLoop(true);									// ������ ����
	m_menuMusic.play();											//

	m_menuButtonSBuffer.loadFromFile("assets/sound/menu_button.wav");	//
	m_menuButtonSound.setBuffer(m_menuButtonSBuffer);					// ����������� ���� ������ ������
	m_menuButtonSound.setVolume(5);										//

	isMousePositionChange = true;	// ����� �� ��������� ������� ������� ����� ��������
	
}

void Menu::draw(RenderWindow & window)
{
																	// ������:
	window.draw(m_backgroundSpr);									// ���
	window.draw(m_logoSpr);											// ����

	window.draw(m_SelectsUnderButton[SELECTED_BUTTON].getRec());	// 
	window.draw(m_SelectsUnderButton[SELECTED_BUTTON].getLine1());	// ��������� ��������� ������
	window.draw(m_SelectsUnderButton[SELECTED_BUTTON].getLine2());	//

	for (auto &button : m_Buttons)									// ��� ��������� ������
		window.draw(button.getButtonText());						//
}

void Menu::setSelectedButton(buttons numOfBut)
{
	if (lastPlayedButtonSound != numOfBut)	// ���� ��� ������ ��� �� ����������
	{										//
		m_menuButtonSound.play();			// �������� ���� ���������
		lastPlayedButtonSound = numOfBut;	// � �������, ��� ��� ������ ��� ����������
	}										//
	SELECTED_BUTTON = (int)numOfBut;		// ������ ��� ������ ���������� ������
}

void Menu::topSelection()
{
	m_menuButtonSound.play();						// ����������� ���� ���������
	if (SELECTED_BUTTON == (int)buttons::NEWGAME)	// ���� �� � ����� ������ ����
		SELECTED_BUTTON = (int)buttons::EXIT;		// ��������� � ����� ����
	else											//
		SELECTED_BUTTON--;							// ����� ���� ������
}

void Menu::downSelection()
{
	m_menuButtonSound.play();						// ����������� ���� ���������
	if (SELECTED_BUTTON == (int)buttons::EXIT)		// ���� �� � ����� ���� ����
		SELECTED_BUTTON = (int)buttons::NEWGAME;	// ��������� � ����� ����
	else											//
		SELECTED_BUTTON++;							// ����� ���� ����
}

void Menu::activated(bool & isMenu,RenderWindow & window)
{								
	switch (SELECTED_BUTTON)		//
	{								//
	case (int)buttons::NEWGAME:		//
		m_Buttons[(int)buttons::NEWGAME].setButtonText(L"����������");
		m_menuMusic.stop();			// ���� ������ "����� ����", �� �� ������ ������ �� ����� � ��������� ����
		isMenu = false;				//
		break;						//
	case (int)buttons::OPTIONS:		//
		break;						//
	case (int)buttons::RATING:		//
		break;						//
	case (int)buttons::CREATORS:	//
		break;						//
	case (int)buttons::EXIT:		//
		window.close();				// ���� ������ "�����", �� �� ��������� ���� 
		break;
	default:
		break;
	}
}

void Menu::update()
{
	//TODO: ����������� � ���� update() ���-�� ��������
}

void Menu::playSound()
{
	m_menuButtonSound.play(); // ����������� ���� ������� ������
}

Menu::~Menu()
{
	// ���� ��� ������ �� �������� ����������
}

Button::Button(sf::String buttonName, float pos_x, float pos_y)
{
	m_buttonFont.loadFromFile("assets/fonts/Ubuntu/Ubuntu-Light.ttf");
	m_buttonText.setFont(m_buttonFont);			// ����� ������
	m_buttonText.setFillColor(Color::White);	// ���� ������
	m_buttonText.setPosition(pos_x, pos_y);		// ������� ������
	m_buttonText.setCharacterSize(32);			// ���������� �����
	m_buttonText.setString(buttonName);			// ������� �� ������
}

void Button::setButtonText(sf::String buttonText)
{
	m_buttonText.setString(buttonText);
}

Text Button::getButtonText()
{
	return m_buttonText;	// ���������� ����� ������(�� ��� ���� ������)
}

Select::Select(Vector2f pos, Vector2f size, Color color)
{
	mainRec.setPosition(pos);						//
	mainRec.setSize(size);							// ������������� ���������
	mainRec.setFillColor(color);					// � ������������ �����
	subLine1.setSize(Vector2f(3, 66));				// (�� ��������� ������)
	subLine1.setPosition(Vector2f(pos.x, pos.y));	//
	subLine2.setSize(Vector2f(3, 66));				//
	subLine2.setPosition(Vector2f(pos.x + 255, pos.y));
}

RectangleShape Select::getRec()
{
	return mainRec;	// ���������� ���������� ������������� ��������
}

RectangleShape Select::getLine1()
{
	return subLine1; // ���������� ����� ����� ����� ��������
}

RectangleShape Select::getLine2()
{
	return subLine2; // ���������� ����� ����� ������ ��������
}
