//#include "stdafx.h"
#include "Engine.h"

void Engine::input()
{
	if (m_isMenu)
	{
		if (Keyboard::isKeyPressed(Keyboard::Escape))	// ������������ ������� Escape
		{
			m_Window.close();
		}
		Event event;
		while (m_Window.pollEvent(event))			// ������ �� �����������
		{
			if(event.type == Event::KeyReleased)	// �������������� ���� ������ ���� �� 
				switch (event.key.code)				// ������ ������, �� 
				{
				case Keyboard::W:
				case Keyboard::Up:
					m_menu.topSelection();			// �������� ������� ������
					break;
				case Keyboard::S:
				case Keyboard::Down:
					m_menu.downSelection();			// �������� ������ ������
					break;
				default:
					break;
				}
		}

		// ����� ����� ����������� ��������� ����� ����
		// ��� ��� ����� �������� �� ��� ��������
		// ����������� ������ ���� �� 5 ������(��������� �����������)
		// TODO: ����������� ����

		if (IntRect(1485, 605, 255, 66).contains(Mouse::getPosition(m_Window))) // ���� ������ ���� �����
		{																		// � ������� ������
			if (m_menu.isMousePositionChange)	// � �� ����� ������� ���� ��������,
			{									// �� �� ���������� ��� ��� ������ ���� ��� ���������
				m_menu.mousePositionAfterSelectButton = (Vector2f)Mouse::getPosition(m_Window);
				m_menu.setSelectedButton(Menu::buttons::NEWGAME); // � �������� ��������������� ������
			}
			if(Mouse::isButtonPressed(Mouse::Left))
				m_menu.activated(m_isMenu, m_Window); // ������ ���� ������
		}

		if (IntRect(1485, 671, 255, 66).contains(Mouse::getPosition(m_Window)))
		{
			if (m_menu.isMousePositionChange)
			{
				m_menu.mousePositionAfterSelectButton = (Vector2f)Mouse::getPosition(m_Window);
				m_menu.setSelectedButton(Menu::buttons::OPTIONS);
			}
			if (Mouse::isButtonPressed(Mouse::Left))
				m_menu.activated(m_isMenu, m_Window);
		}

		if (IntRect(1485, 737, 255, 66).contains(Mouse::getPosition(m_Window)))
		{
			if (m_menu.isMousePositionChange)
			{
				m_menu.mousePositionAfterSelectButton = (Vector2f)Mouse::getPosition(m_Window);
				m_menu.setSelectedButton(Menu::buttons::RATING);
			}
			if (Mouse::isButtonPressed(Mouse::Left))
				m_menu.activated(m_isMenu, m_Window);
		}

		if (IntRect(1485, 803, 255, 66).contains(Mouse::getPosition(m_Window)))
		{
			if (m_menu.isMousePositionChange)
			{
				m_menu.mousePositionAfterSelectButton = (Vector2f)Mouse::getPosition(m_Window);
				m_menu.setSelectedButton(Menu::buttons::CREATORS);
			}
			if (Mouse::isButtonPressed(Mouse::Left))
				m_menu.activated(m_isMenu, m_Window);
		}

		if (IntRect(1485, 869, 255, 66).contains(Mouse::getPosition(m_Window)))
		{
			if (m_menu.isMousePositionChange)
			{
				m_menu.mousePositionAfterSelectButton = (Vector2f)Mouse::getPosition(m_Window);
				m_menu.setSelectedButton(Menu::buttons::EXIT);
			}
			if (Mouse::isButtonPressed(Mouse::Left))
				m_menu.activated(m_isMenu, m_Window);
		}

		if (Keyboard::isKeyPressed(Keyboard::Enter))// ���� ������ ���� ������
		{
			m_menu.activated(m_isMenu, m_Window);  //�������������� ������������ �������
		}
	}
	else
	{
		Event event;
		while (m_Window.pollEvent(event))
		{
			if (event.type == Event::KeyReleased)
				switch (event.key.code)
				{
				case Keyboard::Escape:
					m_isMenu = true;
					break;
				default:
					break;
				}
		}


		if (!m_isGameOver)
		{
			////////////////////////����� ��� ������� �� ������������////////////////////////////////
			bool handlingFirstPlayerClicks = !m_Catapult1.isShooting && m_playerTurn == Player::FIRST;
			bool handlingSecondPlayerClicks = !m_Catapult2.isShooting && m_playerTurn == Player::SECOND;


			if (handlingFirstPlayerClicks && Keyboard::isKeyPressed(Keyboard::W)
				||
				handlingSecondPlayerClicks && Keyboard::isKeyPressed(Keyboard::Up))
			{
				(--m_Stones.end())->addAngle();	// �������� ���� ��������
			}

			if (handlingFirstPlayerClicks && Keyboard::isKeyPressed(Keyboard::S)
				||
				handlingSecondPlayerClicks && Keyboard::isKeyPressed(Keyboard::Down))
			{
				(--m_Stones.end())->subAngle();	// �������� ���� ��������
			}

			if (handlingFirstPlayerClicks && Keyboard::isKeyPressed(Keyboard::A)
				||
				handlingSecondPlayerClicks && Keyboard::isKeyPressed(Keyboard::Left))
			{
				(--m_Stones.end())->subVelocity();	// �������� ��������� �������� �����
			}

			if (handlingFirstPlayerClicks && Keyboard::isKeyPressed(Keyboard::D)
				||
				handlingSecondPlayerClicks && Keyboard::isKeyPressed(Keyboard::Right))
			{
				(--m_Stones.end())->addVelocity();	// �������� ��������� �������� �����
			}

			if (handlingFirstPlayerClicks && Keyboard::isKeyPressed(Keyboard::Space))
			{
				m_Catapult1.isShooting = true; // ����������� ����, ��������� ������� ����������
			}

			if (handlingSecondPlayerClicks && Keyboard::isKeyPressed(Keyboard::Enter))
			{
				m_Catapult2.isShooting = true; // ����������� ����
			}
		}
	}
}
