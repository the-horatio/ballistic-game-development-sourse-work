//#include "stdafx.h"
#include "Engine.h"

void Engine::draw()
{
	m_Window.clear(Color::White);	// ������� ���������� ����
	if (m_isMenu)
	{
		m_menu.draw(m_Window);			// ������������ ����
		m_Window.draw(m_CursorSprite);	// ������������ ������
	}
	else
	{
		m_Window.draw(m_BackgroundSprite);	// ������������ ���
		if (!m_isGameOver)	// ���� ���� ������������
		{
			m_Window.draw(m_Catapult1.getButtonText());	// ������������ ����������
			m_Window.draw(m_Catapult2.getButtonText()); //

			for (auto &element : m_Stones)				//
			{											//
				if (&element != (--m_Stones.end())._Ptr)// ������������ ��� �����
					m_Window.draw(element.getButtonText());	//
			}											//

			if (m_playerTurn == Player::FIRST)	// ������� �������� ���� � ����� � ����������� �� ���� ������
			{									//
				m_Window.draw(m_textAngle1);	// ���������� ���������
				m_Window.draw(m_textVelocity1);	// ������� ������
			}
			else
			{
				m_Window.draw(m_textAngle2);	// ���������� ���������
				m_Window.draw(m_textVelocity2); // ������� ������
			}
		}
		else
		{
			//........................................................
			Texture gameOverTex;
			Sprite gameOverSpr;
			gameOverTex.loadFromFile("assets/img/game_over.png");
			gameOverSpr.setTexture(gameOverTex);				//TODO: ����� ��������� ����
			gameOverSpr.setPosition(Vector2f(740,900));
			m_Window.draw(gameOverSpr);
			//.......................................................
		}
	}
	m_Window.display();	// ���������� ���, ��� ����������
}
