//#include "stdafx.h"
#include "engine.h"

Engine::Engine()
{
	//////////////////////////////////////////////////////////
	Vector2f resolution;									//
	resolution.x = VideoMode::getDesktopMode().width;		//
	resolution.y = VideoMode::getDesktopMode().height;		//
															// �������� ���������� ������, ������� ���� SFML � View
	m_Window.create(VideoMode(resolution.x, resolution.y),	// 
		"BallisticGame",									//
		Style::Fullscreen);									//
	m_Window.setMouseCursorVisible(false);					//	� ������� ������ �����
	//////////////////////////////////////////////////////////
	
	m_BackgroundTexture.loadFromFile("assets/img/background.png");	// ��������� ��� � ��������
	m_BackgroundSprite.setTexture(m_BackgroundTexture);				// ��������� ������ � ��������
	 
	m_FontLight.loadFromFile("assets/fonts/Ubuntu/Ubuntu-Light.ttf");	// ��������� �����(�����)

	m_CursorTexture.loadFromFile("assets/img/mouse_cursor_2.png");	//	�������� ��������,
	m_CursorSprite.setTexture(m_CursorTexture);						//  ���������� �������
	m_CursorSprite.setScale(0.08, 0.08);							//	� ��������������� �������

	/////////// �����������///////////////////////
	m_textAngle1.setFont(m_FontLight);			//
	m_textVelocity1.setFont(m_FontLight);		//
	m_textAngle1.setFillColor(Color::Black);	// ��� ������� ������
	m_textVelocity1.setFillColor(Color::Black);	//
	m_textAngle1.setPosition(100, 50);			//
	m_textVelocity1.setPosition(100, 80);		//
	//////////////////////////////////////////////
	m_textAngle2.setFont(m_FontLight);			//
	m_textVelocity2.setFont(m_FontLight);		//
	m_textAngle2.setFillColor(Color::Black);	// ��� ������� ������
	m_textVelocity2.setFillColor(Color::Black);	//
	m_textAngle2.setPosition(1670, 50);			//
	m_textVelocity2.setPosition(1670, 80);		//
	//////////////////////////////////////////////
	
	m_Stones.push_back(tempStone);	// ��������� ������ ������� � ������

	m_playerTurn = Player::FIRST;	// ������������� ����������� ��� ������� ������

	m_isGameOver = false;			// ����: ���� ����������� ?
	m_isFirstPlayerWon = false;		// ����: ������� ������ ����� ?
	m_isSecondPlayerWon = false;	// ����: ������� ������ ����� ?
	m_isMenu = true;				// ����: ������� �� ���� ?

}

void Engine::start()
{
	Clock clock;	// ������ �������

	while (m_Window.isOpen())
	{
		
		Time dt = clock.restart();	// ������������� ������ � ���������� ���������� ����� � dt

		float dtAsSeconds = dt.asSeconds();	// ����������� ����� � �������
 
		input();				// ��� �������� ������������
		update(dtAsSeconds);	// ��� �� 
		draw();					// ������ ���� ����/������
	}
}

bool Engine::wasCollided(FloatRect first, FloatRect second) const
{
	return first.intersects(second);
}

void Engine::endGame(Player player)
{
	m_isGameOver = true;			// ����������� ����, ��������� ����� ����
	m_BackgroundTexture.loadFromFile("assets/img/end_game_background.png"); // ��������� �������� 
	m_BackgroundSprite.setTexture(m_BackgroundTexture);						// � ��������� ������ 
																			// ���� ����� ����
	if (player == Player::FIRST)
	{
		m_isFirstPlayerWon = true;	// ����������� ����, ��������� ������ ������� ������
	}
	else
	{
		m_isSecondPlayerWon = true;	// ����������� ����, ��������� ������ ������� ������
	}
}

void Engine::shootLogic(Catapult& catapult, float dtAsSeconds)
{
	if (catapult.isShooting) // ���� ������ ���������� ��������
	{
      		(--m_Stones.end())->isFlying = !catapult.shoot(dtAsSeconds);	// ���� ���������� ��������� �������,
																			//  �� ������ �������
																			// �������������� ����������� ����
		if ((--m_Stones.end())->isFlying) // ���� ������ �������
		{
			m_playerTurn = 
				(catapult.getOwner() == Player::FIRST) ? Player::SECOND: Player::FIRST; // ����������� ���� ����

			tempStone.setPosition(m_playerTurn);	// ������������� ������� ����� 
			tempStone.setDirection(m_playerTurn);	// � ����������� ������

			m_Stones.push_back(tempStone); // ����������� ����� ������
		}
	}
}
