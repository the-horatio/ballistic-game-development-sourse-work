#pragma once
#include <SFML/Graphics.hpp>
#include "Players.h"

using namespace sf;

class Catapult
{
private:

	Vector2f m_Position;	//
	Texture m_Texture;		// ����� ��� ��������� ����������� �����
	Sprite m_Sprite;		//

	FloatRect m_Bounds;		// ������� ������� (��� ��������� ������������)

	float m_currentFrame;	// ������� ���� �������� ����������

	Player owner;
public:
	
	Catapult(Player);				// ����������� ���������� � ������������

	/////////////GETTERS//////////////
	Sprite getButtonText() const;	// ��� �������� ������� � ������� �������
									//
	FloatRect getBounds() const;	// ��� ��������� ������� ������������
									//
	Player getOwner() const;		// ���������� ���������, ������� �������� ���������
	//////////////////////////////////

	bool shoot(float dtAsSeconds);	// ����������� �������� �������� ���������� 

	bool isShooting; // ����: ���������� �� ������ ������� ?
};
