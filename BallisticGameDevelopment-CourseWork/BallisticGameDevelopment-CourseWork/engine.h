#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include <functional>
#include "Catapult.h"
#include "Stone.h"
#include "Players.h"
#include "Menu.h"

using namespace sf;

class Engine
{
private:

	RenderWindow m_Window;		// ���� � ������� ���������� �����

	Menu m_menu;				// ��������� ����
	bool m_isMenu;				// ����: � ���� �� �� ������?

	Sprite m_CursorSprite;		// ��������� ������ � �������� ��� �������
	Texture m_CursorTexture;	//

	Sprite m_BackgroundSprite;	// ��������� ������ � �������� ��� ����
	Texture m_BackgroundTexture;//

	Font m_FontLight;			// ��������� �����

	Text m_textAngle1;			//
	Text m_textVelocity1;		// ��������� �������
	Text m_textAngle2;			// ��� �������� ������ ��� ������
	Text m_textVelocity2;		//

	Catapult m_Catapult1 = Catapult(Player::FIRST);		// ��������� ������ ����������
	Catapult m_Catapult2 = Catapult(Player::SECOND);	// ��������� ������ ����������

	Player m_playerTurn;			// ������� � ���, ����� ����� �����

	bool m_isGameOver;				//
	bool m_isFirstPlayerWon;		// ����� ��������� ����
	bool m_isSecondPlayerWon;		//

	std::vector<Stone> m_Stones;	// ������ ���� ������ � ����
	Stone tempStone;				// ���������� �������� ��������� ������ Stone

	void input();					// ������������ ����
	void update(float);				// ������ ���������
	void draw();					// ������������ ����

	void shootLogic(Catapult&, float);	// ����� ��������� ���������� �� ����������
										// ���� �� ����������� ���� ������ ����� � ��������� ����� ������

	void endGame(Player);			// ����� ���������� ��� ���������
									// �� ������ �� �������

	bool wasCollided(FloatRect, FloatRect) const;	// ���������� ��������� �� ������������ 
													// ����� 2-�� ��������
public:

	Engine();		// ����������� ������

	void start();	// ������� ����� ������� ��� ��������� �������

};
