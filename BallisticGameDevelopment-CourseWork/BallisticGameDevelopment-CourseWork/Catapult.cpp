#include "Catapult.h"

Catapult::Catapult(Player player):owner(player)
{
	//////////////////////////////////////////////////////
	m_Texture.loadFromFile("assets/img/Catapult.png");	//
	m_Sprite.setTexture(m_Texture);						// ��������� �������� � ������
	m_Sprite.setTextureRect(IntRect(0, 0, 130, 115));	//
	//////////////////////////////////////////////////////

	if (owner == Player::FIRST)	// ��������� ��� ���������� ������� ������
	{	
		m_Sprite.setScale(1, 1);	// ������������� ����������� ����������
		
		m_Position.x = 0;		// ������������� ��������� ������� 
		m_Position.y = 850;		// ���������� � ��������
	}
	else	// ��������� ��� ���������� ������� ������
	{	
		m_Sprite.setScale(-1, 1); // ������������� ����������� ����������

		m_Position.x = 1900;	// ������������� ��������� ������� 
		m_Position.y = 850;		// ���������� � ��������
	}
		
	m_Sprite.setPosition(m_Position);	// ������������� ������ �� ��������� �������

	m_Bounds = m_Sprite.getGlobalBounds(); // ������������� ������� �������
	
	m_currentFrame = 0.f; // ������������� ���������� � ��������� ���������

	isShooting = false;	// ���� ����������� �� �� ���������� �� �������� �������� (�� � �������)
}

Sprite Catapult::getButtonText() const { return m_Sprite; }

FloatRect Catapult::getBounds() const { return m_Bounds; }

Player Catapult::getOwner() const { return owner; }

bool Catapult::shoot(float dtAsSeconds)
{
	m_currentFrame+= 5 * dtAsSeconds; // ���������� ��������� ����

	if (m_currentFrame < 10) // ���� �� ��� ����� ���������
	{
		m_Sprite.setTextureRect(IntRect(int(m_currentFrame) * 130, 0, 130, 115));// ��������� ����

		return true;// ���������� ��� ��� ��������
	}
	else	// ��� ����� ���������
	{
		isShooting = false; // ���������� ��� �� ��������

		m_currentFrame = 0; // �������� ���������� � ��������� ���������
		m_Sprite.setTextureRect(IntRect(m_currentFrame, 0, 130, 115));

		return false; // ��������� � ����� ��������
	}
}
