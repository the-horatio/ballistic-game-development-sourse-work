#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>

class Button // ����� ������ 
{
private:
	Text m_buttonText; // ������ ������������ �������
	Font m_buttonFont; // ����� ������� ������
public:
	Button(sf::String buttonName, float pos_x, float pos_y); // �������� ����� ������
															 // � �� ��������������
	void setButtonText(sf::String buttonText);
	Text getButtonText(); // ���������� ����� ������
};

class Select // ����� ��� ���������
{
private:
	RectangleShape mainRec;		//
	RectangleShape subLine1;	// ��������� ��� ���������� ������ �������������
	RectangleShape subLine2;	// � ������ ��������� �� �����
public:
	Select(Vector2f pos, Vector2f size, Color color); // �������� ������� ���������, ������ � ����(������������)

	///////////GETTERS////////////
	RectangleShape getRec();	//
	RectangleShape getLine1();	//
	RectangleShape getLine2();	//
	//////////////////////////////
};

class Menu
{
public:
	enum class buttons { NEWGAME, OPTIONS, RATING, CREATORS, EXIT }; // ������������ ����� ������
private:
	
	enum {QUANTITY = 5}; // ���-�� ����� ������
	Button m_Buttons[QUANTITY] = 
	{
		Button(L"����� ����", 1485 + 45, 605 + 13),	//
		Button(L"���������", 1485 + 45, 671 + 13),	// ������ ����� ������
		Button(L"�������", 1485 + 45, 737 + 13),	//
		Button(L"���������", 1485 + 45, 803 + 13),	//
		Button(L"�����", 1485 + 45, 869 + 13)		//
	};
	Select m_SelectsUnderButton[QUANTITY] =
	{
		Select(Vector2f(1485,605), Vector2f(255,66), Color(0,0,0,100)),	//
		Select(Vector2f(1485,671), Vector2f(255,66), Color(0,0,0,100)),	// ������ ���������
		Select(Vector2f(1485,737), Vector2f(255,66), Color(0,0,0,100)),	// ������
		Select(Vector2f(1485,803), Vector2f(255,66), Color(0,0,0,100)),	//
		Select(Vector2f(1485,869), Vector2f(255,66), Color(0,0,0,100))	//
	};
	
	int SELECTED_BUTTON;		// ������� ��������� ������

	Texture m_backgroundTex;	// ��� ����
	Sprite m_backgroundSpr;		//

	Texture m_logoTex;	//	���� � ����
	Sprite m_logoSpr;	//

	Music m_menuMusic;	// ������ � ����

	SoundBuffer m_menuButtonSBuffer;	// ���� ��������� ������
	Sound m_menuButtonSound;			//

	buttons lastPlayedButtonSound;		// ��������� ������, ����� ��������� ������� ����� ����
public:
	Menu(); // ����������� �� ���������
	
	void draw(RenderWindow & window);	// ������������ ����

	void input(RenderWindow & window);	// �������� ��� ����������� input() �� engine

	void setSelectedButton(buttons numOfBut); // ������������� ������� ���������� ������

	void topSelection();	// �������� ������� ������

	void downSelection();	// �������� ������� ������

	void activated(bool & isMenu, RenderWindow & window); // ��� ��������� ������� �� ������

	void update();	// ��������� ������� ��������� � ����

	void playSound();	// ��������� ���� ��������� ������
	
	Vector2f mousePositionAfterSelectButton;	// ������� ���� ����� ��������� ������

	bool isMousePositionChange;	// ����: ���� �� �������� ������� ����?

	~Menu();	// ������ ����������
};

