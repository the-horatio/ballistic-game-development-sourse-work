//#include "stdafx.h"
#include "Engine.h"
#include <sstream>

using namespace sf;

void Engine::update(float dtAsSeconds)
{
	if (m_isMenu)
	{
		m_CursorSprite.setPosition(static_cast<Vector2f>(Mouse::getPosition(m_Window)));	// ������� �������
																							// ��������� c �������� ����
		if (m_menu.mousePositionAfterSelectButton == m_CursorSprite.getPosition())	// ���� ������� ���� 
			m_menu.isMousePositionChange = false;									// �� ��������
		else																		// ����������� ����
			m_menu.isMousePositionChange = true;									// 
	}
	else
	{
		if (!m_isGameOver)
		{
			std::ostringstream m_angleString;		// ������ ��� ������ ����� �� ����� (� Window)
			std::ostringstream m_velocityString;	//

			m_angleString << (--m_Stones.end())->getAngle();		// ������� ������ ��� ������ � ������
			m_velocityString << (--m_Stones.end())->getVelocity();	//

			if (m_playerTurn == Player::FIRST)	// ����� ���������� ������� ������
			{
				m_textAngle1.setString("Angle: " + m_angleString.str());			// ����������� ������ �� ������� � ������ 
				m_textVelocity1.setString("Velocity: " + m_velocityString.str());	// � ������� � ������� ��� ������ ������
			}
			else	// ����� ���������� ������� ������
			{
				m_textAngle2.setString("Angle: " + m_angleString.str());			// ����������� ������ �� ������� � ������
				m_textVelocity2.setString("Velocity: " + m_velocityString.str());	// � ������� � ������� ��� ������ ������
			}

			shootLogic(m_Catapult1, dtAsSeconds);	// ��������� ������
			shootLogic(m_Catapult2, dtAsSeconds);	// ���������� ������

			for (auto &element : m_Stones)			// ���������� �� ���� ��������
				if (element.isFlying)				// ���� ������ �����
					element.flight(dtAsSeconds);	// ������������ ����� �����

			if (m_Stones.size() > 1)
			{
				if (m_playerTurn == Player::SECOND && m_Stones[m_Stones.size() - 2].isFlying)
				{
					if (wasCollided(m_Stones[m_Stones.size() - 2].getBounds(), m_Catapult2.getBounds())) 
					{								// ���� ��� �������� ������ �����
						endGame(Player::FIRST);		// ����������� ���� � ��������� �������
					}
				}
				else if (m_playerTurn == Player::FIRST && (--(--m_Stones.end()))->isFlying)
				{
					if (wasCollided((--(--m_Stones.end()))->getBounds(), m_Catapult1.getBounds()))
					{								// ���� ��� �������� ������ �����
						endGame(Player::SECOND);	// ����������� ���� � ��������� �������
					}
				}
			}
		}
	}
}
