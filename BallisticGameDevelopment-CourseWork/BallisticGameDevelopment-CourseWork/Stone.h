#pragma once
#include <SFML/Graphics.hpp>
#include "Players.h"
using namespace sf;

class Stone
{
private:
	
	Vector2f m_Position;	//
	Texture m_Texture;		// ����� ��� ��������� ����������� �����
	Sprite m_Sprite;		//

	FloatRect m_Bounds;		// ������� ������� ��� ��������� ������������

	double m_Angle;			// ���� ������
	double m_Velocity;		// ��������� �������� ������

	Player m_currentDirection; // ���������� ����������� ������
	
	float m_Time;			// ����� ������ �����
	float m_currentFrame;	// ������� ���� �������� �����

public:

	Stone();				// ������� �����������
	
	//////////GETTERS/////////////////
	Sprite getButtonText() const;		// ���������� ����������� �����, ������������ � Draw
									//
	FloatRect getBounds() const;	// �������� ������� �����
									//
	int getAngle() const;			// ��� �������� ���� � ������� �������
									//
	int getVelocity() const;		// ��� �������� �������� � ������� �������
	//////////////////////////////////

	//////////SETTERS/////////////
	void setDirection(Player);	// ���������� ����������� ������
								//
	void setPosition(Player);	// ���������� ��������� ����� (��� �������� ����)
	//////////////////////////////
	
	void addAngle();		// ��������� ���� �� �������

	void subAngle();		// ��������� ���� �� �������

	void addVelocity();		// ��������� �������� ����� �� �������

	void subVelocity();		// ��������� �������� ����� �� �������

	void flight(float dtAsSeconds);	// ���������� ���� ��� ������ ����� (�������� � �������)
	
	bool isFlying;	// ���� ��� ���� ����� ����� � ����� ��������� ������

};

